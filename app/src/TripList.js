import React, { Component } from 'react';
import { Button, ButtonGroup, Container, Table } from 'reactstrap';
import AppNavbar from './AppNavBar';
import { Link } from 'react-router-dom';

class TripList extends Component {

  constructor(props) {
    super(props);
    this.state = {trips: [], isLoading: true};
  }

  componentDidMount() {
    this.setState({isLoading: true});

    fetch('taxi/user/trips/2')
      .then(response => response.json())
      .then(data => this.setState({trips: data, isLoading: false}));
  }


  render() {
    const {trips, isLoading} = this.state;

    if (isLoading) {
      return <p>Loading...</p>;
    }

    const tripList = trips.map(trip => {
    
      return <tr key={trip.id}>
    	<td style={{whiteSpace: 'nowrap'}}>{trip.id}</td>
      	<td style={{whiteSpace: 'nowrap'}}>{trip.user.firstName}</td>
        <td>{trip.taxi.registrationNumber}</td>
      </tr>
    });

    return (
      <div>
        <AppNavbar/>
        <Container fluid>
          <div className="float-right">
            <Button color="success" tag={Link} to="/groups/new">Add Group</Button>
          </div>
          <h3>My Trips</h3>
          <Table className="mt-4">
            <thead>
            <tr>
              <th width="20%">Trip id</th>
              <th width="20%">User</th>
              <th>Taxi</th>
            </tr>
            </thead>
            <tbody>
            {tripList}
            </tbody>
          </Table>
        </Container>
      </div>
    );
  }
}

export default TripList;