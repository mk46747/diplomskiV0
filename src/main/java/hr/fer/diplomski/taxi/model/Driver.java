package hr.fer.diplomski.taxi.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("person_driver") 
public class Driver extends Person{

	private static final long serialVersionUID = 1L;
	
	@Column(name = "driver_licence_number")
	private String licenceNumber;
	
	public String getLicenceNumber() {
		return licenceNumber;
	}

	public void setLicenceNumber(String licenceNumber) {
		this.licenceNumber = licenceNumber;
	}
	
	
}
