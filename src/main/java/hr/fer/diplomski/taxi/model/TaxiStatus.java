package hr.fer.diplomski.taxi.model;

import java.util.HashMap;
import java.util.Map;

public enum TaxiStatus {
	
	FREE("free"),
	BUSY("busy");
	
	private static final Map<String, TaxiStatus> LOOKUP = new HashMap<String, TaxiStatus>();
	
	static {
		for (final TaxiStatus taxiStatus : TaxiStatus.values()) {
			LOOKUP.put(taxiStatus.getCode(), taxiStatus);
		}
	}	
	
	public static TaxiStatus getTaxiStatus(final String p_code) {
		return LOOKUP.get(p_code);
	}

	private String m_code;

	private TaxiStatus(final String p_code) {
		m_code = p_code;
	}

	public String getCode() {
		return m_code;
	}

	public void setCode(final String p_code) {
		m_code = p_code;
	}

}
