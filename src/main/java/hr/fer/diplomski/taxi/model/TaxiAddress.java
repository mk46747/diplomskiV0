package hr.fer.diplomski.taxi.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name = "tx_taxi_address")
public class TaxiAddress {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_tx_taxi_address")
	@SequenceGenerator(name = "seq_tx_taxi_address", sequenceName = "seq_tx_taxi_address", allocationSize = 1)
	@Column(name = "taxi_address_id", nullable = false)
	private Long id;
	
	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "taxi_id")
	private Taxi taxi;
	
	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "address_id")
	private Address address;
	
	@Column(name = "trip_start_time", nullable = false)
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime time;

}
