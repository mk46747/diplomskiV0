package hr.fer.diplomski.taxi.model;

import java.util.HashMap;
import java.util.Map;

public enum Currency {
	
	KUNA("kn"),
	EURO("euro");
	
	private static final Map<String, Currency> LOOKUP = new HashMap<String, Currency>();
	
	static {
		for (final Currency currency : Currency.values()) {
			LOOKUP.put(currency.getCode(), currency);
		}
	}	
	
	public static Currency getCurrency(final String p_code) {
		return LOOKUP.get(p_code);
	}

	private String m_code;

	private Currency(final String p_code) {
		m_code = p_code;
	}

	public String getCode() {
		return m_code;
	}

	public void setCode(final String p_code) {
		m_code = p_code;
	}

}
