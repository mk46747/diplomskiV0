package hr.fer.diplomski.taxi.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tx_address")
public class Address {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_tx_address")
	@SequenceGenerator(name = "seq_tx_address", sequenceName = "seq_tx_address", allocationSize = 1)
	@Column(name = "address_id", nullable = false)
	private Long id;

	@Column(name = "address_latitude", nullable = false)
	private long latitude;
	
	@Column(name = "address_longitude", nullable = false)
	private long longitude;
	
	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "city_id")
	private City city;
	
	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "country_id")
	private Country country;
	
	@Column(name = "address_street")
	private String street;
	
	@Column(name = "address_house_num")
	private String houseNumber;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public long getLatitude() {
		return latitude;
	}

	public void setLatitude(long latitude) {
		this.latitude = latitude;
	}

	public long getLongitude() {
		return longitude;
	}

	public void setLongitude(long longitude) {
		this.longitude = longitude;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}
	
	
}
