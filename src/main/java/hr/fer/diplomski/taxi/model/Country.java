package hr.fer.diplomski.taxi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
@Entity
@Table(name = "tx_country")
public class Country {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_tx_cuntry")
	@SequenceGenerator(name = "seq_tx_cuntry", sequenceName = "seq_tx_cuntry", allocationSize = 1)
	@Column(name = "country_id", nullable = false)
	private Long id;

	@Column(name = "country_name", nullable = false)
	private String name;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
