package hr.fer.diplomski.taxi.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tx_taxi")
public class Taxi implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_tx_taxi")
	@SequenceGenerator(name = "seq_tx_taxi", sequenceName = "seq_tx_taxi", allocationSize = 1)
	@Column(name = "taxi_id", nullable = false)
	private Long id;
	
	@OneToOne(cascade = { CascadeType.ALL }, orphanRemoval = true)
	@JoinColumn(name = "per_id")
	private Driver driver;

	@Column(name = "taxi_reg_number", nullable = false)
	private String registrationNumber;
	
	@Column(name = "taxi_vehicle_model", nullable = false)
	private String vehicleModel;
	
//	@OneToOne(cascade = { CascadeType.ALL }, orphanRemoval = true)
//	@JoinColumn(name = "address_id")
//	private Address location;
	
	@Enumerated(EnumType.STRING)
	private TaxiStatus taxiStatus;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Driver getDriver() {
		return driver;
	}

	public void setDriver(Driver driver) {
		this.driver = driver;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	public String getVehicleModel() {
		return vehicleModel;
	}

	public void setVehicleModel(String vehicleModel) {
		this.vehicleModel = vehicleModel;
	}


	public TaxiStatus getTaxiStatus() {
		return taxiStatus;
	}

	public void setTaxiStatus(TaxiStatus taxiStatus) {
		this.taxiStatus = taxiStatus;
	} 
	
	
	
	
}
