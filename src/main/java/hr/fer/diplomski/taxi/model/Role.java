package hr.fer.diplomski.taxi.model;

import java.util.HashMap;
import java.util.Map;

public enum Role {

	ROLE_USER("role_user"),
	ROLE_ADMIN("role_admin");
	
	private static final Map<String, Role> LOOKUP = new HashMap<String, Role>();
	
	static {
		for (final Role role : Role.values()) {
			LOOKUP.put(role.getCode(), role);
		}
	}	
	
	public static Role getRole(final String p_code) {
		return LOOKUP.get(p_code);
	}

	private String m_code;

	private Role(final String p_code) {
		m_code = p_code;
	}

	public String getCode() {
		return m_code;
	}

	public void setCode(final String p_code) {
		m_code = p_code;
	}
}
