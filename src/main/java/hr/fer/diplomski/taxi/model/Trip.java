package hr.fer.diplomski.taxi.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name = "tx_trip")
public class Trip implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_tx_trip")
	@SequenceGenerator(name = "seq_tx_trip", sequenceName = "seq_tx_trip", allocationSize = 1)
	@Column(name = "trip_id", nullable = false)
	private Long id;
	
	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "per_id")
	private User user;
	
	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "taxi_id")
	private Taxi taxi;
	
	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "address_start_id")
	private Address start;
	
	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "address_dest_id")
	private Address destination;
	
	@Column(name = "trip_distance", nullable = false)
	private long distance;
	
	@Column(name = "trip_start_time", nullable = false)
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime startTime;
	
	@Column(name = "trip_end_time", nullable = false)
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime endTime;
	
//	@OneToOne(cascade = { CascadeType.ALL }, orphanRemoval = true)
//	@JoinColumn(name = "fare_id")
//	private Fare fare;
	
	@OneToOne(cascade = { CascadeType.ALL }, orphanRemoval = true)
	@JoinColumn(name = "fpl_id")
	private FarePriceList farePriceList;
	
	@Enumerated(EnumType.STRING)
	private TripStatus tripStatus;
	
	@Column(name = "trip_base_fare", nullable = false)
	private BigDecimal baseFare;
	
	@Column(name = "trip_time_fare", nullable = false)
	private BigDecimal timeFare;
	
	@Column(name = "trip_distance_fare", nullable = false)
	private BigDecimal distanceFare;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Taxi getTaxi() {
		return taxi;
	}

	public void setTaxi(Taxi taxi) {
		this.taxi = taxi;
	}

	public Address getStart() {
		return start;
	}

	public void setStart(Address start) {
		this.start = start;
	}

	public Address getDestination() {
		return destination;
	}

	public void setDestination(Address destination) {
		this.destination = destination;
	}

	public long getDistance() {
		return distance;
	}

	public void setDistance(long distance) {
		this.distance = distance;
	}

	public DateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(DateTime startTime) {
		this.startTime = startTime;
	}

	public DateTime getEndTime() {
		return endTime;
	}

	public void setEndTime(DateTime endTime) {
		this.endTime = endTime;
	}



	public BigDecimal getBaseFare() {
		return baseFare;
	}

	public void setBaseFare(BigDecimal baseFare) {
		this.baseFare = baseFare;
	}

	public BigDecimal getTimeFare() {
		return timeFare;
	}

	public void setTimeFare(BigDecimal timeFare) {
		this.timeFare = timeFare;
	}

	public BigDecimal getDistanceFare() {
		return distanceFare;
	}

	public void setDistanceFare(BigDecimal distanceFare) {
		this.distanceFare = distanceFare;
	}

	public FarePriceList getFarePriceList() {
		return farePriceList;
	}

	public void setFarePriceList(FarePriceList farePriceList) {
		this.farePriceList = farePriceList;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TripStatus getTripStatus() {
		return tripStatus;
	}

	public void setTripStatus(TripStatus tripStatus) {
		this.tripStatus = tripStatus;
	}
	
	
	
	
}
