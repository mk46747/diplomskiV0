//package hr.fer.diplomski.taxi.model;
//
//import java.io.Serializable;
//import java.math.BigDecimal;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.EnumType;
//import javax.persistence.Enumerated;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.SequenceGenerator;
//import javax.persistence.Table;
//
//@Entity
//@Table(name = "tx_fare")
//public class Fare implements Serializable{
//
//	private static final long serialVersionUID = 1L;
//
//	@Id
//	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_tx_fare")
//	@SequenceGenerator(name = "seq_tx_fare", sequenceName = "seq_tx_fare", allocationSize = 1)
//	@Column(name = "fare_id", nullable = false)
//	private Long id;
//	
//	@Column(name = "fare_base_fare", nullable = false)
//	private BigDecimal baseFare;
//	
//	@Column(name = "fare_time_fare", nullable = false)
//	private BigDecimal timeFare;
//	
//	@Column(name = "fare_distance_fare", nullable = false)
//	private BigDecimal distanceFare;
//	
//	@Column(name = "fare_currency")
//	@Enumerated(EnumType.STRING)
//	private Currency currency;
//	
//	public Long getId() {
//		return id;
//	}
//
//	public void setId(Long id) {
//		this.id = id;
//	}
//
//	public BigDecimal getBaseFare() {
//		return baseFare;
//	}
//
//	public void setBaseFare(BigDecimal baseFare) {
//		this.baseFare = baseFare;
//	}
//
//	public BigDecimal getTimeFare() {
//		return timeFare;
//	}
//
//	public void setTimeFare(BigDecimal timeFare) {
//		this.timeFare = timeFare;
//	}
//
//	public BigDecimal getDistanceFare() {
//		return distanceFare;
//	}
//
//	public void setDistanceFare(BigDecimal distanceFare) {
//		this.distanceFare = distanceFare;
//	}
//
//	public Currency getCurrency() {
//		return currency;
//	}
//
//	public void setCurrency(Currency currency) {
//		this.currency = currency;
//	}
//	
//	
//
//}
