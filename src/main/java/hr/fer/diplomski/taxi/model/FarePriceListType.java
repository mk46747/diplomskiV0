package hr.fer.diplomski.taxi.model;

import java.util.HashMap;
import java.util.Map;

public enum FarePriceListType {

	NORMAL("normal"),
	HIGH("high");
	
	private static final Map<String, FarePriceListType> LOOKUP = new HashMap<String, FarePriceListType>();
	
	static {
		for (final FarePriceListType farePriceListType : FarePriceListType.values()) {
			LOOKUP.put(farePriceListType.getCode(), farePriceListType);
		}
	}	
	
	public static FarePriceListType getFarePriceListType(final String p_code) {
		return LOOKUP.get(p_code);
	}

	private String m_code;

	private FarePriceListType(final String p_code) {
		m_code = p_code;
	}

	public String getCode() {
		return m_code;
	}

	public void setCode(final String p_code) {
		m_code = p_code;
	}
}
