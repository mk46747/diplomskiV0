package hr.fer.diplomski.taxi.service;

import java.util.List;

import javax.transaction.Transactional;

import hr.fer.diplomski.taxi.model.Trip;
import hr.fer.diplomski.taxi.model.User;

@Transactional //TODO
public interface UserService {
	
	User findByUsername(String username);
	
	User findByUsernameAndPassword(String username, String password);
	
	User findById(Long id);
	
	User create(User user);
	
	User update(User user);
	
	void delete(Long id);
	
	List<Trip> getTripsForUser(long userId);
	
	boolean existsByUsername(String username);

	
}
