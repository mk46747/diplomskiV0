package hr.fer.diplomski.taxi.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import hr.fer.diplomski.taxi.model.Trip;
import hr.fer.diplomski.taxi.model.User;
import hr.fer.diplomski.taxi.repository.TripRepository;
import hr.fer.diplomski.taxi.repository.UserRepository;
import hr.fer.diplomski.taxi.service.UserService;

@Service
public class UserServiceImpl implements UserService{
	
	@SuppressWarnings("unused")
	private final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

	private final TripRepository tripRepository;
	
	private final UserRepository userRepository;
	
	public UserServiceImpl(UserRepository userRepository, TripRepository tripRepository) {
		this.userRepository = userRepository;
		this.tripRepository = tripRepository;
	}
	
	@Override
	public User findByUsername(String username) {
		return userRepository.findByUsername(username);
	}
	

	@Override
	public User findByUsernameAndPassword(String username, String password) {
		return userRepository.findByUsernameAndPassword(username, password);
	}

	@Override
	public User findById(Long id) {
		return userRepository.getOne(id);
	}

	@Override
	public User create(User user) {
		return userRepository.save(user);
		
	}

	@Override
	public User update(User user) {
		return userRepository.save(user);
		
	}

	@Override
	public void delete(Long id) {
		userRepository.deleteById(id);
		
	}

	@Override
	public List<Trip> getTripsForUser(long userId) {
		return tripRepository.findByUserId(userId);
	}

	@Override
	public boolean existsByUsername(String username) {
		return userRepository.existsByUsername(username);
	}




}
