package hr.fer.diplomski.taxi.service;

import hr.fer.diplomski.taxi.model.Address;
import hr.fer.diplomski.taxi.model.Trip;

public interface OrderExecutionService {

	Trip orderRide(Long userId, Address start, Address destination);
	
	Trip startRide(Long tripId);
	
	Trip finishRide(Long tripId);
}
