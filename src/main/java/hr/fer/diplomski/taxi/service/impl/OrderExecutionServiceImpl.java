package hr.fer.diplomski.taxi.service.impl;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import hr.fer.diplomski.taxi.model.Address;
import hr.fer.diplomski.taxi.model.Taxi;
import hr.fer.diplomski.taxi.model.TaxiStatus;
import hr.fer.diplomski.taxi.model.Trip;
import hr.fer.diplomski.taxi.model.TripStatus;
import hr.fer.diplomski.taxi.model.User;
import hr.fer.diplomski.taxi.repository.TaxiRepository;
import hr.fer.diplomski.taxi.repository.TripRepository;
import hr.fer.diplomski.taxi.repository.UserRepository;
import hr.fer.diplomski.taxi.service.BillingService;
import hr.fer.diplomski.taxi.service.OrderExecutionService;

@Service
public class OrderExecutionServiceImpl implements OrderExecutionService{
	
	@SuppressWarnings("unused")
	private final Logger LOGGER = LoggerFactory.getLogger(OrderExecutionServiceImpl.class);
	
	private final TripRepository tripRepository;
	
	private final TaxiRepository taxiRepository;
	
	private final UserRepository userRepository;
	
	private final BillingService billingService;
	
	public OrderExecutionServiceImpl(TripRepository tripRepository, TaxiRepository taxiRepository, 
			UserRepository userRepository, BillingService billingService) {
		this.tripRepository = tripRepository;
		this.taxiRepository = taxiRepository;
		this.userRepository = userRepository;
		this.billingService = billingService;
	}
	
	@Override
	public Trip orderRide(Long userId, Address start, Address destination) {
		Taxi taxi = findClosestTaxi(start);
		User user = userRepository.getOne(userId);
		
		Trip trip = new Trip();
		trip.setUser(user);
		trip.setTaxi(taxi);
		trip.setStart(start);
		trip.setDestination(destination);
		//TODO
		trip.setDistance(25);
		trip.setTripStatus(TripStatus.PENDING);
		
		return tripRepository.save(trip);		
	}

	@Override
	public Trip startRide(Long tripId) {
		Trip trip = tripRepository.getOne(tripId);
		trip.setStartTime(DateTime.now());
		trip.setTripStatus(TripStatus.STARTED);
		
		return tripRepository.save(trip);		
	}

	@Override
	public Trip finishRide(Long tripId) {
		Trip trip = tripRepository.getOne(tripId);
		trip.setEndTime(DateTime.now());
		trip.setTripStatus(TripStatus.FINISHED);
		//trip.setFare(billingService.calculateFare(trip));
		trip.setFarePriceList(billingService.getFarePriceList());
		Taxi taxi = taxiRepository.getOne(trip.getTaxi().getId());
		taxi.setTaxiStatus(TaxiStatus.FREE);
		
		return tripRepository.save(trip);

	}
	
	//TODO closest
	private Taxi findClosestTaxi(Address start){
		Taxi taxi = taxiRepository.findFirstByTaxiStatus(TaxiStatus.FREE);
		return taxi;
	}
	

}
