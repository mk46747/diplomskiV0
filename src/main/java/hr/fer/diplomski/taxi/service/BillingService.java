package hr.fer.diplomski.taxi.service;

import hr.fer.diplomski.taxi.model.FarePriceList;
import hr.fer.diplomski.taxi.model.Trip;

public interface BillingService {
	
	byte[] generatePdfReceipt(Long tripId);
	
	void calculateFare(Trip trip);

	FarePriceList getFarePriceList();

}
