package hr.fer.diplomski.taxi.service.impl;

import java.math.BigDecimal;

import org.joda.time.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import hr.fer.diplomski.taxi.model.FarePriceList;
import hr.fer.diplomski.taxi.model.Trip;
import hr.fer.diplomski.taxi.repository.FarePriceListRepository;
import hr.fer.diplomski.taxi.service.BillingService;

@Service
public class BillingServiceImpl implements BillingService{
	
	@SuppressWarnings("unused")
	private final Logger LOGGER = LoggerFactory.getLogger(BillingServiceImpl.class);
	
	private final FarePriceListRepository farePriceListRepository;
	
	public BillingServiceImpl(FarePriceListRepository farePriceListRepository) {
		this.farePriceListRepository = farePriceListRepository;
	}
	
	@Override
	public byte[] generatePdfReceipt(Long tripId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void calculateFare(Trip trip) {
		FarePriceList fpl = getFarePriceList();
		
//		Fare fare = new Fare();
//		
//		fare.setCurrency(fpl.getCurrency());
//		fare.setBaseFare(fpl.getBaseFarePrice());
//		
//		BigDecimal distanceFare = fpl.getDistanceFarePricePerKm().multiply(new BigDecimal(trip.getDistance()));
//		fare.setDistanceFare(distanceFare);
//		
//		Duration tripDuration = new Duration(trip.getStartTime(), trip.getEndTime());
//		long tripDurationInMinutes = tripDuration.getStandardMinutes();
//		BigDecimal timeFare = fpl.getTimeFarePricePerMin().multiply(new BigDecimal(tripDurationInMinutes));
//		fare.setTimeFare(timeFare);
		
		return;
		
	}

	@Override
	public FarePriceList getFarePriceList() {
		return farePriceListRepository.findByActive(Boolean.TRUE);
	}

}
