package hr.fer.diplomski.taxi.jasper;

import hr.fer.diplomski.taxi.TestDataUtils;
import hr.fer.diplomski.taxi.model.Trip;

public class TestFactoryUtil {
	public static java.util.Collection<Trip> generateCollection() {
		final java.util.Vector<Trip> collection = new java.util.Vector<Trip>();
		collection.add(generateTrip());

		return collection;

	}

	private static Trip generateTrip() {
		return TestDataUtils.createTrip();
	}
}
