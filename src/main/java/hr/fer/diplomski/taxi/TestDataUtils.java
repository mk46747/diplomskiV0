package hr.fer.diplomski.taxi;

import java.math.BigDecimal;

import org.joda.time.DateTime;

import hr.fer.diplomski.taxi.model.Address;
import hr.fer.diplomski.taxi.model.City;
import hr.fer.diplomski.taxi.model.Country;
import hr.fer.diplomski.taxi.model.Currency;
import hr.fer.diplomski.taxi.model.Driver;
import hr.fer.diplomski.taxi.model.FarePriceList;
import hr.fer.diplomski.taxi.model.FarePriceListType;
import hr.fer.diplomski.taxi.model.Taxi;
import hr.fer.diplomski.taxi.model.Trip;
import hr.fer.diplomski.taxi.model.User;

public class TestDataUtils {
	
	public static Trip createTrip(){
		User user = new User();
		user.setFirstName("TestName");
		user.setLastName("LastName");
		user.setEmail("test@test.com");
		user.setPassword("testPassword");
		user.setUsername("TestUsername");
		user.setPhoneNumber("123456789");
		
		Driver driver = new Driver();
		driver.setFirstName("driverName");
		driver.setLastName("driverName");
		driver.setEmail("driver@test.com");
		driver.setPhoneNumber("111111111");
		
		City city = new City();
		city.setName("Zagreb");
		
		Country country = new Country();
		country.setName("Hrvatska");
		
		Address address = new Address();
		address.setLatitude(0);
		address.setLongitude(0);
		address.setCountry(country);
		address.setCity(city);
		address.setStreet("Unska");
		address.setHouseNumber("3");
		
		Taxi taxi = new Taxi();
		taxi.setDriver(driver);
		//taxi.setLocation(location);
		taxi.setRegistrationNumber("zg-500");
		taxi.setVehicleModel("Renault 4");
		
//		Fare fare = new Fare();
//		fare.setBaseFare(new BigDecimal(1.23));
//		fare.setCurrency(Currency.KUNA);
//		fare.setDistanceFare(new BigDecimal(11.1));
//		fare.setTimeFare(new BigDecimal(14.11));
		

		
		FarePriceList fpl = new FarePriceList();
		fpl.setBaseFarePrice(new BigDecimal(5));
		fpl.setCurrency(Currency.KUNA);
		fpl.setDistanceFarePricePerKm(new BigDecimal(56));
		fpl.setFarePriceListType(FarePriceListType.NORMAL);
		fpl.setTimeFarePricePerMin(new BigDecimal(78));
		
		
		Trip trip = new Trip();
		trip.setDestination(address);
		trip.setDistance(100);
		trip.setEndTime(new DateTime());
		//trip.setFare(fare);
		trip.setFarePriceList(fpl);
		trip.setStart(address);
		trip.setStartTime(new DateTime());
		trip.setTaxi(taxi);
		trip.setUser(user);
		
		trip.setBaseFare(new BigDecimal(1.23));
		trip.setDistanceFare(new BigDecimal(11.1));
		trip.setTimeFare(new BigDecimal(14.11));
		
		return trip;
	}
}
