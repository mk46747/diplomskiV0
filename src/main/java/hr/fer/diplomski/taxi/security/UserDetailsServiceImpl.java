//package hr.fer.diplomski.taxi.security;
//
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.stereotype.Service;
//
//import hr.fer.diplomski.taxi.model.User;
//import hr.fer.diplomski.taxi.service.UserService;
//
//@Service
//public class UserDetailsServiceImpl implements UserDetailsService {
//
//	private final UserService userService;
//
//	public UserDetailsServiceImpl(final UserService userService) {
//		this.userService = userService;
//	}
//
//	@Override
//	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//		User user = userService.findByUsername(username);
//
//		if (user == null) {
//			throw new UsernameNotFoundException("User not found with username: " + username);
//		}
//
//		return UserPrincipal.create(user);
//	}
//
//	public UserDetails loadUserById(Long id) {
//		User user = userService.findById(id);
//
//		if (user == null) {
//			throw new UsernameNotFoundException("User not found with id: " + id);
//		}
//		
//		return UserPrincipal.create(user);
//	}
//
//}
