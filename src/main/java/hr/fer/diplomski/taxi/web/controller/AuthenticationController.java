//package hr.fer.diplomski.taxi.web.controller;
//
//import java.net.URI;
//
//import javax.validation.Valid;
//
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
//
//import hr.fer.diplomski.taxi.auth.payload.ApiResponse;
//import hr.fer.diplomski.taxi.auth.payload.JwtAuthenticationResponse;
//import hr.fer.diplomski.taxi.auth.payload.LoginRequest;
//import hr.fer.diplomski.taxi.auth.payload.RegistrationRequest;
//import hr.fer.diplomski.taxi.model.Role;
//import hr.fer.diplomski.taxi.model.User;
//import hr.fer.diplomski.taxi.security.JwtTokenProvider;
//import hr.fer.diplomski.taxi.service.UserService;
//
//@RestController
//@RequestMapping("/taxi/auth")
//public class AuthenticationController {
//
//	private final AuthenticationManager authenticationManager;
//	
//	private final UserService userService;
//	
//	private final PasswordEncoder passwordEncoder;
//	
//	private final JwtTokenProvider tokenProvider;
//
//	public AuthenticationController(AuthenticationManager authenticationManager, UserService userService,
//			PasswordEncoder passwordEncoder, JwtTokenProvider tokenProvider) {
//		this.authenticationManager = authenticationManager;
//		this.userService = userService;
//		this.passwordEncoder = passwordEncoder;
//		this.tokenProvider = tokenProvider;
//	}
//	
//	@PostMapping("/login")
//	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest){
//		
//		Authentication authentication = authenticationManager.authenticate(
//				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
//		
//		SecurityContextHolder.getContext().setAuthentication(authentication);
//		String jwt = tokenProvider.generateToken(authentication);
//		return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
//	}
//	
//	@PostMapping("/register")
//	public ResponseEntity<?> registerUser(@Valid @RequestBody RegistrationRequest registrationRequest){
//		if(userService.existsByUsername(registrationRequest.getUsername())){
//			return new ResponseEntity<>(new ApiResponse(false, "Username is already taken"), HttpStatus.BAD_REQUEST);
//		}
//		
//		User user = createUserFromRequest(registrationRequest);
//		User result = userService.create(user);
//		
//		 URI location = ServletUriComponentsBuilder
//	                .fromCurrentContextPath().path("/taxi/user/{id}")
//	                .buildAndExpand(result.getId()).toUri();
//
//	        return ResponseEntity.created(location).body(new ApiResponse(true, "User registered successfully"));
//
//	}
//
//	private User createUserFromRequest(RegistrationRequest registrationRequest) {
//		User user = new User();
//		user.setFirstName(registrationRequest.getFirstName());
//		user.setLastName(registrationRequest.getLastName());
//		user.setPassword(passwordEncoder.encode(registrationRequest.getPassword()));
//		user.setPhoneNumber(registrationRequest.getPhoneNumber());
//		user.setRole(Role.ROLE_USER);
//		return user;
//	}
//	
//	
//}
