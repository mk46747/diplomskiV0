package hr.fer.diplomski.taxi.web.controller;

import java.util.Collection;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.fer.diplomski.taxi.model.Trip;
import hr.fer.diplomski.taxi.model.User;
import hr.fer.diplomski.taxi.service.UserService;

@RestController
@RequestMapping("/taxi")
public class UserController {
	
	private final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
	
	private final UserService userService;
	
	
	public UserController(final UserService userService){
		this.userService = userService;
	}
	
	@GetMapping("/user/{id}")
	ResponseEntity<?> getUser(@PathVariable Long id){
		
		LOGGER.info("Request to find user with id: {}", id);
		User user = userService.findById(id);
		if(user != null){
			LOGGER.info("Found user with id: {} ", id);
			return ResponseEntity.ok().body(user);	
		}
		LOGGER.warn("Did not find user with id: {} ", id);
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	
//	@PostMapping("/user")
//	ResponseEntity<User> createUser(@Valid @RequestBody User user){
//		LOGGER.info("Request to create user: {}", user);
//		User result = userService.create(user);
//		return ResponseEntity.ok().body(result);
//	}
	
	@PutMapping("/user/{id}")
	ResponseEntity<User> updateUser(@PathVariable Long id, @Valid @RequestBody User user){
		user.setId(id);
		LOGGER.info("Request to update user: {}", user);
		User result = userService.update(user);
		return ResponseEntity.ok().body(result);
	}
	
	@DeleteMapping("/user/{id}")
	ResponseEntity<User> deleteUser(@PathVariable Long id){
		LOGGER.info("Request to delete user with id: {}", id);
		userService.delete(id);
		return ResponseEntity.ok().build();
	}
	
	@GetMapping("/user/trips/{id}")
	Collection<Trip> getUserTrips(@PathVariable Long id){
		
		return userService.getTripsForUser(id);

	}

}
