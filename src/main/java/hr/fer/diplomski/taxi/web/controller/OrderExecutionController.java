package hr.fer.diplomski.taxi.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.fer.diplomski.taxi.model.Address;
import hr.fer.diplomski.taxi.model.Trip;
import hr.fer.diplomski.taxi.service.OrderExecutionService;

@RestController
@RequestMapping("/taxi/ride")
public class OrderExecutionController {
	
	private final Logger LOGGER = LoggerFactory.getLogger(OrderExecutionController.class);
	
	private final OrderExecutionService orderExecutionService;
		
	public OrderExecutionController(final OrderExecutionService orderExecutionService){
		this.orderExecutionService = orderExecutionService;
	}
	
	@GetMapping("")
	ResponseEntity<?> homePage(){
		
		return ResponseEntity.ok().body("");	

	}
	
	@PostMapping("/order")
	ResponseEntity<Trip> orderRide(){
		LOGGER.info("Request order ride");
		//TODO
		Trip trip = orderExecutionService.orderRide(Long.valueOf(0), new Address(), new Address());
		return ResponseEntity.ok().body(trip);
	}
	
	@PostMapping("/start/{tripId}")
	ResponseEntity<Trip> startRide(@PathVariable Long tripId){
		LOGGER.info("Request start ride");
		//TODO
		Trip trip = orderExecutionService.startRide(tripId);
		return ResponseEntity.ok().body(trip);
	}
	
	@PostMapping("/finish/{tripId}")
	ResponseEntity<Trip> finishRide(@PathVariable Long tripId){
		LOGGER.info("Request finish ride");
		//TODO
		Trip trip = orderExecutionService.finishRide(tripId);
		return ResponseEntity.ok().body(trip);
	}
	

	

}
