package hr.fer.diplomski.taxi.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hr.fer.diplomski.taxi.service.BillingService;

@RestController
@RequestMapping("/taxi/billing")
public class BillingController {
	private final Logger LOGGER = LoggerFactory.getLogger(BillingController.class);
	
	
	private final BillingService billingService;
	
	public BillingController(final BillingService billingService){
		this.billingService = billingService;
	}
	
	@PostMapping("/receipt/{tripId}")
	ResponseEntity<byte[]> getReceipt(@PathVariable Long tripId){
		LOGGER.info("Request finish ride");
		//TODO
		byte[] recipt = billingService.generatePdfReceipt(tripId);
		return ResponseEntity.ok().body(recipt);
	}
}
