package hr.fer.diplomski.taxi;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import hr.fer.diplomski.taxi.model.Trip;
import hr.fer.diplomski.taxi.repository.TripRepository;
import hr.fer.diplomski.taxi.repository.UserRepository;

@Component
public class Initializer implements CommandLineRunner {
	
	@SuppressWarnings("unused")
	private final UserRepository userRepository;
	private final TripRepository tripRepository;
	
	public Initializer(UserRepository userRepository, TripRepository tripRepository) {
		this.userRepository = userRepository;
		this.tripRepository = tripRepository;
	}

	@Override
	public void run(String... args) throws Exception {
		Trip trip = TestDataUtils.createTrip();
		
//		userRepository.save(user);
//		
//		userRepository.findAll().forEach(System.out::println);
		
		tripRepository.save(trip);
		
	}
	
	

}
